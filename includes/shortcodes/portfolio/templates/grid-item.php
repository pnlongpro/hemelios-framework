<div class="portfolio-item <?php echo esc_attr($cat_filter) ?>">

	<?php
	$post_id           = get_the_ID();
	$post_thumbnail_id = get_post_thumbnail_id(  get_the_ID() );
	$title = get_post_meta( $post_id, 'custom_project_name', true );
	$link_url = get_post_meta( $post_id, 'project_url', true );
	$arrImages = wp_get_attachment_image_src( $post_thumbnail_id, 'full' );
	$width = 480;
	$height = 600;
	$thumbnail_url = '';
	if(count($arrImages)>0){
		$resize = matthewruddy_image_resize($arrImages[0],$width,$height);
		if($resize!=null && is_array($resize) )
			$thumbnail_url = $resize['url'];
	}

	$url_origin = $arrImages[0];

	if( ! $title  ){
		$title = get_the_title($post_id);
	}

	?>
	<figure class="entry-thumbnail">
		<span class="image-bg">
			<span class="image-shop-scroll" style="background-image: url('<?php echo esc_url($url_origin) ?>');"></span>
			<span class="glare"></span>
		</span>
		<div class="__entry-thumbnail-hover">
			<div class="__entry-hover-wrapper">
				<div class="__entry-hover-inner">
					<a target="_blank" rel="nofollow"  href="<?php echo $link_url ?>" class="ico-view-detail">
						<i class="fa fa-plus"></i>
					</a>
				</div>
			</div>
		</div>
		<div class="__entry-content">
			<h3 class="fig-title">
				<?php
				echo esc_html($title);
				?>
			</h3>
			<p><?php echo esc_html($link_url) ?></p>

		</div>
	</figure>


</div>
