<?php
// don't load directly
if (!defined('ABSPATH')) die('-1');
if (!class_exists('g5plusFramework_Shortcode_Icon_Box')) {
	class g5plusFramework_Shortcode_Icon_Box
	{
		function __construct()
		{
			add_shortcode('os_icon_box', array($this, 'icon_box_shortcode'));
		}

		function icon_box_shortcode($atts)
		{
			$layout_type = $title_style = $icon_type = $image = $color_style = $icon_style = $layout_style = $link = $description = $title = $symbol_bg =
			$icon = $html = $el_class =  $style_icon = $bg_color = $box_style =
			$g5plus_animation = $css_animation = $duration = $delay = $styles_animation = '';
			extract(shortcode_atts(array(
				'layout_type' => 'style1',
				'layout_style' => 'istyle-top',
				'color_style' => 'istyle-inherit',
				'icon_type' => 'font-icon',
				'icon' => '',
				'icon_style' => 'simple-icon',
				'bg_color' => '',
				'image' => '',
				'link' => '',
				'title' => '',
				'title_style' => 'no-border',
				'description' => '',
				'symbol_bg' => '',
				'el_class' => '',
				'css_animation' => '',
				'duration' => '',
				'delay' => ''
			), $atts));
			$custom_data = array();
			$box_css = array();
			$g5plus_animation .= ' ' . esc_attr($el_class);
			$g5plus_animation .= g5plusFramework_Shortcodes::g5plus_get_css_animation($css_animation);

			//parse link
			$link = ($link == '||') ? '' : $link;
			$link = vc_build_link($link);
			if( $bg_color ){
				$box_css[] = 'background-color: '. $bg_color .'';
			}
			if( $box_css ){
				$box_style = 'style="'. join(';',$box_css).'"';
			}
            $prelink = $endlink = '';
			if( $title_style == 'border-bottom' ){
				$title_class = 'border-bottom';
			}else{
				$title_class = 'no-border';
			}
			if (strlen($link['url']) > 0) {
				$a_href = $link['url'];
				$a_title = $link['title'];
				$a_target = strlen($link['target']) > 0 ? $link['target'] : '_self';
                $prelink = '<a title="'.esc_attr($a_title).'" target="'.trim(esc_attr($a_target)).'" href="'.esc_url($a_href).'">';
                $endlink = '</a>';
			}

			ob_start();?>
			<div <?php echo $box_style ?> class="os-ibox clearfix <?php echo esc_attr($layout_style . ' ' . $layout_type) . ' ' . esc_attr($color_style); ?> <?php echo esc_attr($g5plus_animation)?>"
				<?php echo g5plusFramework_Shortcodes::g5plus_get_style_animation($duration, $delay); ?>>
				<?php
					if( $layout_type == 'style3' ) :
				?>
					<div class="__iconbox-wrapper">
						<figure>
							<div class="overlay"></div>
							<div class="__icon">
								<?php if ($icon_type == 'font-icon') : ?>
									<i class="<?php echo esc_attr($icon) ?>"></i>
									<?php
								else :
									$img = wp_get_attachment_image_src($image, 'full');
									?>
									<img src="<?php echo esc_attr($img[0]) ?>"/>
								<?php endif; ?>
								<?php if ($title != ''): ?>
									<h3 class="__title <?php echo esc_attr($title_class); ?>">
										<?php echo $prelink ?>
										<?php echo esc_html($title) ?>
										<?php echo $endlink ?>
									</h3>
								<?php endif;?>
							</div>
							<figcaption>
								<?php if ($title != ''): ?>
									<h3 class="__title <?php echo esc_attr($title_class); ?>">
										<?php echo $prelink ?>
										<?php echo esc_html($title) ?>
										<?php echo $endlink ?>
									</h3>
								<?php endif;?>
								<?php if ($description != ''):?>
									<div class="__inner-content">
										<p><?php echo wp_kses_post($description) ?></p>
									</div>
								<?php endif; ?>
							</figcaption>
						</figure>
					</div>

				<?php else : ?>
				<?php echo $prelink ?>
				<?php echo $endlink ?>
				<div class="__header clearfix">
					<div class="__icon <?php echo esc_attr($icon_style) ?>">
                            <?php if ($icon_type == 'font-icon') : ?>
                                <i class="<?php echo esc_attr($icon) ?>"></i>
                                <?php
                            else :
                                $img = wp_get_attachment_image_src($image, 'full');
                                ?>
                                <img src="<?php echo esc_attr($img[0]) ?>"/>
                            <?php endif; ?>
					</div>
					<?php if ($title != ''): ?>
						<h3 class="__title <?php echo esc_attr($title_class); ?>">
							<?php echo $prelink ?>
								<?php echo esc_html($title) ?>
							<?php echo $endlink ?>
						</h3>
					<?php endif;?>
				</div>
				<?php if ($description != ''):?>
				<div class="__content">
					<div class="__entry_content">
						<div class="__inner-content">

								<?php if ($title != ''): ?>
									<h3 class="__title <?php echo esc_attr($title_class); ?>">
										<?php echo $prelink ?>
										<?php echo esc_html($title) ?>
										<?php echo $endlink ?>
									</h3>
								<?php endif;?>
								<p><?php echo wp_kses_post($description) ?></p>

						</div>
					</div>
				</div>
				<?php endif; ?>
				<?php if($symbol_bg != ''): ?>
					<div class="__symbol-bg"><?php echo esc_html($symbol_bg) ;?></div>
				<?php endif ?>
			<?php endif; ?>
			</div>

			<?php
			$content = ob_get_clean();
			return $content;
		}
	}

	new g5plusFramework_Shortcode_Icon_Box();
}