<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 5/28/2015
 * Time: 5:44 PM
 */

if (!class_exists('g5plusFramework_Shortcodes')) {
    class g5plusFramework_Shortcodes
    {

        private static $instance;

        public static function init()
        {
            if (!isset(self::$instance)) {
                self::$instance = new g5plusFramework_Shortcodes;
                add_action('init', array(self::$instance, 'includes'), 0);
                add_action('init', array(self::$instance, 'register_vc_param'), 5);
                add_action('init', array(self::$instance, 'register_vc_map'), 10);
            }

            return self::$instance;
        }

        public function includes()
        {
            include_once(ABSPATH . 'wp-admin/includes/plugin.php');
            if (!is_plugin_active('js_composer/js_composer.php')) {
                return;
            }
            global $g5plus_options;
            $cpt_disable = $g5plus_options['cpt-disable'];
            include_once(PLUGIN_G5PLUS_FRAMEWORK_DIR . 'includes/shortcodes/slider-container/slider-container.php');
            include_once(PLUGIN_G5PLUS_FRAMEWORK_DIR . 'includes/shortcodes/heading/heading.php');
            include_once(PLUGIN_G5PLUS_FRAMEWORK_DIR . 'includes/shortcodes/icon-home/icon-home.php');
            include_once(PLUGIN_G5PLUS_FRAMEWORK_DIR . 'includes/shortcodes/button/button.php');
            include_once(PLUGIN_G5PLUS_FRAMEWORK_DIR . 'includes/shortcodes/icon-box/icon-box.php');
            include_once(PLUGIN_G5PLUS_FRAMEWORK_DIR . 'includes/shortcodes/partner-carousel/partner-carousel.php');
            include_once(PLUGIN_G5PLUS_FRAMEWORK_DIR . 'includes/shortcodes/post/post.php');
            include_once(PLUGIN_G5PLUS_FRAMEWORK_DIR . 'includes/shortcodes/feature/feature.php');
            include_once(PLUGIN_G5PLUS_FRAMEWORK_DIR . 'includes/shortcodes/testimonial/testimonial.php');
            include_once(PLUGIN_G5PLUS_FRAMEWORK_DIR . 'includes/shortcodes/counter/counter.php');
            include_once(PLUGIN_G5PLUS_FRAMEWORK_DIR . 'includes/shortcodes/video-bg/video-bg.php');
            include_once(PLUGIN_G5PLUS_FRAMEWORK_DIR . 'includes/shortcodes/notification/notification.php');
            include_once(PLUGIN_G5PLUS_FRAMEWORK_DIR . 'includes/shortcodes/call-to-action/call-to-action.php');
            include_once(PLUGIN_G5PLUS_FRAMEWORK_DIR . 'includes/shortcodes/gallery/gallery.php');
            include_once(PLUGIN_G5PLUS_FRAMEWORK_DIR . 'includes/shortcodes/download/download.php');
            include_once(PLUGIN_G5PLUS_FRAMEWORK_DIR . 'includes/shortcodes/product/product.php');
            include_once(PLUGIN_G5PLUS_FRAMEWORK_DIR . 'includes/shortcodes/product/product-categories.php');
            include_once(PLUGIN_G5PLUS_FRAMEWORK_DIR . 'includes/shortcodes/countdown/countdown.php');
            include_once(PLUGIN_G5PLUS_FRAMEWORK_DIR . 'includes/shortcodes/support/support.php');
            include_once(PLUGIN_G5PLUS_FRAMEWORK_DIR . 'includes/shortcodes/timeline/timeline.php');

            if (!isset($cpt_disable) || $cpt_disable['ourteam'] == '0' || $cpt_disable['ourteam'] == '') {
                include_once(PLUGIN_G5PLUS_FRAMEWORK_DIR . 'includes/shortcodes/ourteam/ourteam.php');
            }
            if (!isset($cpt_disable) || $cpt_disable['portfolio'] == '0' || $cpt_disable['portfolio'] == '') {
                include_once(PLUGIN_G5PLUS_FRAMEWORK_DIR . 'includes/shortcodes/portfolio/portfolio.php');
            }
            if (!isset($cpt_disable) || $cpt_disable['services'] == '0' || $cpt_disable['services'] == '') {
                include_once(PLUGIN_G5PLUS_FRAMEWORK_DIR . 'includes/shortcodes/services/services.php');
            }


        }

        public static function g5plus_get_css_animation($css_animation)
        {
            $output = '';
            if ($css_animation != '') {
                wp_enqueue_script('waypoints');
                $output = ' wpb_animate_when_almost_visible g5plus-css-animation ' . $css_animation;
            }

            return $output;
        }

        public static function g5plus_get_style_animation($duration, $delay)
        {
            $styles = array();
            if ($duration != '0' && !empty($duration)) {
                $duration = (float)trim($duration, "\n\ts");
                $styles[] = "-webkit-animation-duration: {$duration}s";
                $styles[] = "-moz-animation-duration: {$duration}s";
                $styles[] = "-ms-animation-duration: {$duration}s";
                $styles[] = "-o-animation-duration: {$duration}s";
                $styles[] = "animation-duration: {$duration}s";
            }
            if ($delay != '0' && !empty($delay)) {
                $delay = (float)trim($delay, "\n\ts");
                $styles[] = "opacity: 0";
                $styles[] = "-webkit-animation-delay: {$delay}s";
                $styles[] = "-moz-animation-delay: {$delay}s";
                $styles[] = "-ms-animation-delay: {$delay}s";
                $styles[] = "-o-animation-delay: {$delay}s";
                $styles[] = "animation-delay: {$delay}s";
            }
            if (count($styles) > 1) {
                return 'style="' . implode(';', $styles) . '"';
            }

            return implode(';', $styles);
        }

        public static function g5plus_convert_hex_to_rgba($hex, $opacity = 1)
        {
            $hex = str_replace("#", "", $hex);
            if (strlen($hex) == 3) {
                $r = hexdec(substr($hex, 0, 1) . substr($hex, 0, 1));
                $g = hexdec(substr($hex, 1, 1) . substr($hex, 1, 1));
                $b = hexdec(substr($hex, 2, 1) . substr($hex, 2, 1));
            } else {
                $r = hexdec(substr($hex, 0, 2));
                $g = hexdec(substr($hex, 2, 2));
                $b = hexdec(substr($hex, 4, 2));
            }
            $rgba = 'rgba(' . $r . ',' . $g . ',' . $b . ',' . $opacity . ')';

            return $rgba;
        }

        public static function substr($str, $txt_len, $end_txt = '...')
        {
            if (empty($str)) {
                return '';
            }
            if (strlen($str) <= $txt_len) {
                return $str;
            }

            $i = $txt_len;
            while ($str[$i] != ' ') {
                $i--;
                if ($i == -1) {
                    break;
                }
            }
            while ($str[$i] == ' ') {
                $i--;
                if ($i == -1) {
                    break;
                }
            }

            return substr($str, 0, $i + 1) . $end_txt;
        }

        public function register_vc_param()
        {

        }

        public function register_vc_map()
        {

            global $g5plus_options;

            $cpt_disable = $g5plus_options['cpt-disable'];

            if (function_exists('vc_map')) {
                $title_style = array(
                    'type' => 'dropdown',
                    'heading' => __('Title Style', 'hemelios'),
                    'param_name' => 'title_style',
                    'value' => array(__('No Border', 'hemelios') => 'no-border',
                        __('Border Bottom', 'hemelios') => 'border-bottom'),
                    'description' => __('Select title style', 'hemelios'),
                );
                $add_css_animation = array(
                    'type' => 'dropdown',
                    'heading' => __('CSS Animation', 'hemelios'),
                    'param_name' => 'css_animation',
                    'value' => array(__('No', 'hemelios') => '', __('Fade In', 'hemelios') => 'wpb_fadeIn', __('Fade Top to Bottom', 'hemelios') => 'wpb_fadeInDown', __('Fade Bottom to Top', 'hemelios') => 'wpb_fadeInUp', __('Fade Left to Right', 'hemelios') => 'wpb_fadeInLeft', __('Fade Right to Left', 'hemelios') => 'wpb_fadeInRight', __('Bounce In', 'hemelios') => 'wpb_bounceIn', __('Bounce Top to Bottom', 'hemelios') => 'wpb_bounceInDown', __('Bounce Bottom to Top', 'hemelios') => 'wpb_bounceInUp', __('Bounce Left to Right', 'hemelios') => 'wpb_bounceInLeft', __('Bounce Right to Left', 'hemelios') => 'wpb_bounceInRight', __('Zoom In', 'hemelios') => 'wpb_zoomIn', __('Flip Vertical', 'hemelios') => 'wpb_flipInX', __('Flip Horizontal', 'hemelios') => 'wpb_flipInY', __('Bounce', 'hemelios') => 'wpb_bounce', __('Flash', 'hemelios') => 'wpb_flash', __('Shake', 'hemelios') => 'wpb_shake', __('Pulse', 'hemelios') => 'wpb_pulse', __('Swing', 'hemelios') => 'wpb_swing', __('Rubber band', 'hemelios') => 'wpb_rubberBand', __('Wobble', 'hemelios') => 'wpb_wobble', __('Tada', 'hemelios') => 'wpb_tada'),
                    'description' => __('Select type of animation if you want this element to be animated when it enters into the browsers viewport. Note: Works only in modern browsers.', 'hemelios'),
                    'group' => __('Animation Settings', 'hemelios')
                );

                $add_duration_animation = array(
                    'type' => 'textfield',
                    'heading' => __('Animation Duration', 'hemelios'),
                    'param_name' => 'duration',
                    'value' => '',
                    'description' => __('Duration in seconds. You can use decimal points in the value. Use this field to specify the amount of time the animation plays. <em>The default value depends on the animation, leave blank to use the default.</em>', 'hemelios'),
                    'dependency' => array('element' => 'css_animation', 'value' => array('wpb_fadeIn', 'wpb_fadeInDown', 'wpb_fadeInUp', 'wpb_fadeInLeft', 'wpb_fadeInRight', 'wpb_bounceIn', 'wpb_bounceInDown', 'wpb_bounceInUp', 'wpb_bounceInLeft', 'wpb_bounceInRight', 'wpb_zoomIn', 'wpb_flipInX', 'wpb_flipInY', 'wpb_bounce', 'wpb_flash', 'wpb_shake', 'wpb_pulse', 'wpb_swing', 'wpb_rubberBand', 'wpb_wobble', 'wpb_tada')),
                    'group' => __('Animation Settings', 'hemelios')
                );

                $add_delay_animation = array(
                    'type' => 'textfield',
                    'heading' => __('Animation Delay', 'hemelios'),
                    'param_name' => 'delay',
                    'value' => '',
                    'description' => __('Delay in seconds. You can use decimal points in the value. Use this field to delay the animation for a few seconds, this is helpful if you want to chain different effects one after another above the fold.', 'hemelios'),
                    'dependency' => array('element' => 'css_animation', 'value' => array('wpb_fadeIn', 'wpb_fadeInDown', 'wpb_fadeInUp', 'wpb_fadeInLeft', 'wpb_fadeInRight', 'wpb_bounceIn', 'wpb_bounceInDown', 'wpb_bounceInUp', 'wpb_bounceInLeft', 'wpb_bounceInRight', 'wpb_zoomIn', 'wpb_flipInX', 'wpb_flipInY', 'wpb_bounce', 'wpb_flash', 'wpb_shake', 'wpb_pulse', 'wpb_swing', 'wpb_rubberBand', 'wpb_wobble', 'wpb_tada')),
                    'group' => __('Animation Settings', 'hemelios')
                );

                $add_el_class = array(
                    'type' => 'textfield',
                    'heading' => __('Extra class name', 'hemelios'),
                    'param_name' => 'el_class',
                    'description' => __('If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'hemelios'),
                );

                $target_arr = array(
                    __('Same window', 'hemelios') => '_self',
                    __('New window', 'hemelios') => '_blank'
                );

                $colors_arr = array(
                    __('hemelios Color', 'hemelios') => 'os_color',
                    __('Grey', 'hemelios') => 'wpb_button',
                    __('Blue', 'hemelios') => 'btn-primary',
                    __('Turquoise', 'hemelios') => 'btn-info',
                    __('Green', 'hemelios') => 'btn-success',
                    __('Orange', 'hemelios') => 'btn-warning',
                    __('Red', 'hemelios') => 'btn-danger',
                    __('Black', 'hemelios') => "btn-inverse"
                );
                //Slider Container
                vc_map(array(
                    'name' => __('Slider Container', 'hemelios'),
                    'base' => 'os_slider_container',
                    'class' => '',
                    'icon' => 'fa fa-ellipsis-h',
                    'category' => G5PLUS_FRAMEWORK_SHORTCODE_CATEGORY,
                    'as_parent' => array('except' => 'os_slider_container'),
                    'content_element' => true,
                    'show_settings_on_create' => true,
                    'params' => array(
                        array(
                            'type' => 'checkbox',
                            'heading' => __('Navigation', 'hemelios'),
                            'param_name' => 'navigation',
                            'description' => __('Show navigation.', 'hemelios'),
                            'value' => array(__('Yes, please', 'hemelios') => 'yes'),
                            'edit_field_class' => 'vc_col-sm-6 vc_column'
                        ),
                        array(
                            'type' => 'checkbox',
                            'heading' => __('Pagination', 'hemelios'),
                            'param_name' => 'pagination',
                            'description' => __('Show pagination.', 'hemelios'),
                            'value' => array(__('Yes, please', 'hemelios') => 'yes'),
                            'std' => 'yes',
                            'edit_field_class' => 'vc_col-sm-6 vc_column'
                        ),
                        array(
                            'type' => 'checkbox',
                            'heading' => __('Single Item', 'hemelios'),
                            'param_name' => 'singleitem',
                            'description' => __('Display only one item.', 'hemelios'),
                            'value' => array(__('Yes, please', 'hemelios') => 'yes'),
                            'edit_field_class' => 'vc_col-sm-6 vc_column'
                        ),
                        array(
                            'type' => 'checkbox',
                            'heading' => __('Stop On Hover', 'hemelios'),
                            'param_name' => 'stoponhover',
                            'description' => __('Stop autoplay on mouse hover.', 'hemelios'),
                            'value' => array(__('Yes, please', 'hemelios') => 'yes'),
                            'edit_field_class' => 'vc_col-sm-6 vc_column'
                        ),
                        array(
                            'type' => 'textfield',
                            'heading' => __('Auto Play', 'hemelios'),
                            'param_name' => 'autoplay',
                            'description' => __('Change to any integer for example autoPlay : 5000 to play every 5 seconds. If you set autoPlay: true default speed will be 5 seconds.', 'hemelios'),
                            'value' => '',
                            'std' => 'true'
                        ),
                        array(
                            'type' => 'textfield',
                            'heading' => __('Items', 'hemelios'),
                            'param_name' => 'items',
                            'description' => __('This variable allows you to set the maximum amount of items displayed at a time with the widest browser width', 'hemelios'),
                            'value' => '',
                            'std' => 4
                        ),
                        array(
                            'type' => 'textfield',
                            'heading' => __('Items Desktop', 'hemelios'),
                            'param_name' => 'itemsdesktop',
                            'description' => __('This allows you to preset the number of slides visible with a particular browser width. The format is [x,y] whereby x=browser width and y=number of slides displayed. For example [1199,4] means that if(window<=1199){ show 4 slides per page} Alternatively use itemsDesktop: false to override these settings.', 'hemelios'),
                            'value' => '',
                            'std' => '1199,4'
                        ),
                        array(
                            'type' => 'textfield',
                            'heading' => __('Items Desktop Small', 'hemelios'),
                            'param_name' => 'itemsdesktopsmall',
                            'value' => '',
                            'std' => '979,3'
                        ),
                        array(
                            'type' => 'textfield',
                            'heading' => __('Items Tablet', 'hemelios'),
                            'param_name' => 'itemstablet',
                            'value' => '',
                            'std' => '768,2'
                        ),
                        array(
                            'type' => 'textfield',
                            'heading' => __('Items Tablet Small', 'hemelios'),
                            'param_name' => 'itemstabletsmall',
                            'value' => '',
                            'std' => 'false'
                        ),
                        array(
                            'type' => 'textfield',
                            'heading' => __('Items Mobile', 'hemelios'),
                            'param_name' => 'itemsmobile',
                            'value' => '',
                            'std' => '479,1'
                        ),
                        array(
                            'type' => 'checkbox',
                            'heading' => __('Items Scale Up', 'hemelios'),
                            'param_name' => 'itemsscaleup',
                            'description' => __('Option to not stretch items when it is less than the supplied items.', 'hemelios'),
                            'value' => array(__('Yes, please', 'hemelios') => 'yes'),
                            'edit_field_class' => 'vc_col-sm-6 vc_column'
                        ),
                        array(
                            'type' => 'checkbox',
                            'heading' => __('Auto Height', 'hemelios'),
                            'param_name' => 'autoheight',
                            'description' => __('You can use different heights on slides.', 'hemelios'),
                            'value' => array(__('Yes, please', 'hemelios') => 'yes'),
                            'edit_field_class' => 'vc_col-sm-6 vc_column'
                        ),
                        array(
                            'type' => 'textfield',
                            'heading' => __('Slide Speed', 'hemelios'),
                            'param_name' => 'slidespeed',
                            'description' => __('Slide speed in milliseconds. Ex 200', 'hemelios'),
                            'value' => '',
                            'std' => '200',
                        ),
                        array(
                            'type' => 'textfield',
                            'heading' => __('Pagination Speed', 'hemelios'),
                            'param_name' => 'paginationspeed',
                            'description' => __('Pagination speed in milliseconds. Ex 800', 'hemelios'),
                            'value' => '',
                            'std' => '800',
                        ),
                        array(
                            'type' => 'textfield',
                            'heading' => __('Rewind Speed', 'hemelios'),
                            'param_name' => 'rewindspeed',
                            'description' => __('Rewind speed in milliseconds. Ex 1000', 'hemelios'),
                            'value' => '',
                            'std' => '1000',
                        ),
                        $add_el_class,
                        $add_css_animation,
                        $add_duration_animation,
                        $add_delay_animation
                    ),
                    'js_view' => 'VcColumnView'
                ));
                // Testimonials
                vc_map(array(
                    'name' => __('Testimonials', 'hemelios'),
                    'base' => 'os_testimonial_ctn',
                    'class' => '',
                    'icon' => 'fa fa-quote-left',
                    'category' => G5PLUS_FRAMEWORK_SHORTCODE_CATEGORY,
                    'as_parent' => array('only' => 'os_testimonial_sc'),
                    'content_element' => true,
                    'show_settings_on_create' => true,
                    'params' => array(
                        array(
                            'type' => 'dropdown',
                            'heading' => __('Layout Style', 'hemelios'),
                            'param_name' => 'layout_style',
                            'admin_label' => true,
                            'value' => array(__('style 1', 'hemelios') => 'style1',
                                __('style 2', 'hemelios') => 'style2'),
                            'description' => __('Select Layout Style.', 'hemelios')
                        ),
                        array(
                            'type' => 'textfield',
                            'heading' => __('Auto Play', 'hemelios'),
                            'param_name' => 'autoplay',
                            'description' => __('Change to any integer for example autoPlay : 5000 to play every 5 seconds. If you set autoPlay: true default speed will be 5 seconds.', 'hemelios'),
                            'value' => '',
                            'std' => 'true'
                        ),
                        array(
                            'type' => 'checkbox',
                            'heading' => __('Stop On Hover', 'hemelios'),
                            'param_name' => 'stoponhover',
                            'description' => __('Stop autoplay on mouse hover.', 'hemelios'),
                            'value' => array(__('Yes, please', 'hemelios') => 'yes'),
                            'edit_field_class' => 'vc_col-sm-6 vc_column'
                        ),
                        array(
                            'type' => 'checkbox',
                            'heading' => __('Auto Height', 'hemelios'),
                            'param_name' => 'autoheight',
                            'description' => __('You can use different heights on slides.', 'hemelios'),
                            'value' => array(__('Yes, please', 'hemelios') => 'yes'),
                            'edit_field_class' => 'vc_col-sm-6 vc_column'
                        ),
                        array(
                            'type' => 'textfield',
                            'heading' => __('Slide Speed', 'hemelios'),
                            'param_name' => 'slidespeed',
                            'description' => __('Slide speed in milliseconds. Ex 200', 'hemelios'),
                            'value' => '',
                            'std' => '200',
                        ),
                        array(
                            'type' => 'textfield',
                            'heading' => __('Pagination Speed', 'hemelios'),
                            'param_name' => 'paginationspeed',
                            'description' => __('Pagination speed in milliseconds. Ex 800', 'hemelios'),
                            'value' => '',
                            'std' => '800',
                        ),
                        array(
                            'type' => 'textfield',
                            'heading' => __('Rewind Speed', 'hemelios'),
                            'param_name' => 'rewindspeed',
                            'description' => __('Rewind speed in milliseconds. Ex 1000', 'hemelios'),
                            'value' => '',
                            'std' => '1000',
                        ),
                        array(
                            'type' => 'textfield',
                            'heading' => __('Number of item', 'hemelios'),
                            'param_name' => 'number_item',
                            'value' => '1',
                        ),
                        array(
                            'type' => 'checkbox',
                            'heading' => __('Show navigation control', 'hemelios'),
                            'param_name' => 'navigation',
                            'value' => array(__('Yes, please', 'hemelios') => 'yes'),
                            'edit_field_class' => 'vc_col-sm-6 vc_column'
                        ),
                        array(
                            'type' => 'checkbox',
                            'heading' => __('Show pagination control', 'hemelios'),
                            'param_name' => 'pagination',
                            'value' => array(__('Yes, please', 'hemelios') => 'yes'),
                            'edit_field_class' => 'vc_col-sm-6 vc_column'
                        ),
                        $add_el_class,
                        $add_css_animation,
                        $add_duration_animation,
                        $add_delay_animation
                    ),
                    'js_view' => 'VcColumnView'
                ));
                // Testimonial
                vc_map(array(
                    'name' => __('Testimonial', 'hemelios'),
                    'base' => 'os_testimonial_sc',
                    'class' => '',
                    'icon' => 'fa fa-user',
                    'category' => G5PLUS_FRAMEWORK_SHORTCODE_CATEGORY,
                    'as_child' => array('only' => 'os_testimonial_ctn', 'os_slider_container'),
                    'params' => array(
                        array(
                            'type' => 'textfield',
                            'heading' => __('Title', 'hemelios'),
                            'param_name' => 'title',
                            'description' => __('Enter title.', 'hemelios')
                        ),
                        $title_style,
                        array(
                            'type' => 'colorpicker',
                            'heading' => __('Background', 'hemelios'),
                            'param_name' => 'bg_testimonials',
                            'value' => '',
                        ),
                        array(
                            'type' => 'textfield',
                            'heading' => __('Author name', 'hemelios'),
                            'param_name' => 'author',
                            'admin_label' => true,
                            'description' => __('Enter Author name.', 'hemelios')
                        ),
                        array(
                            'type' => 'textfield',
                            'heading' => __('Author information', 'hemelios'),
                            'param_name' => 'author_info',
                            'description' => __('Enter author information.', 'hemelios')
                        ),
                        array(
                            'type' => 'attach_image',
                            'heading' => __('Author Avata', 'hemelios'),
                            'param_name' => 'author_avata',
                            'description' => __('Select image from library.', 'hemelios'),
                            'dependence' => array(
                                'param' => ""
                            )
                        ),
                        array(
                            'type' => 'textarea',
                            'heading' => __('Quote from author', 'hemelios'),
                            'param_name' => 'content',
                            'value' => ''
                        ),
                        array(
                            'type' => 'colorpicker',
                            'heading' => __('Quote Color', 'hemelios'),
                            'param_name' => 'quote_color',
                            'value' => '',
                        ),
                    )
                ));
                // Video Background
                vc_map(array(
                    'name' => __('Video Background', 'hemelios'),
                    'base' => 'os_video_bg',
                    'class' => '',
                    'icon' => 'fa fa-video-camera',
                    'category' => G5PLUS_FRAMEWORK_SHORTCODE_CATEGORY,
                    'params' => array(
                        array(
                            'type' => 'dropdown',
                            'heading' => __('Layout Style', 'hemelios'),
                            'param_name' => 'layout_style',
                            'admin_label' => true,
                            'value' => array(__('style 1', 'hemelios') => 'style1'),
                            'description' => __('Select Layout Style.', 'hemelios')
                        ),
                        array(
                            'type' => 'textarea',
                            'heading' => __('Link Video (.mp4 or .ogg)', 'hemelios'),
                            'param_name' => 'video_link',
                            'value' => '',
                        ),
                        array(
                            'type' => 'attach_image',
                            'heading' => __('Upload Image:', 'hemelios'),
                            'param_name' => 'image',
                            'value' => '',
                            'description' => __('Image show on mobile device and when not autoplay mode.', 'hemelios'),
                        ),
                        array(
                            'type' => 'textfield',
                            'heading' => __('Title', 'hemelios'),
                            'param_name' => 'title',
                            'value' => '',
                        ),
                        array(
                            'type' => 'textarea',
                            'heading' => __('Description', 'hemelios'),
                            'param_name' => 'description',
                            'value' => '',
                        ),
                        array(
                            'type' => 'checkbox',
                            'heading' => __('Video Autoplay', 'hemelios'),
                            'param_name' => 'autoplay',
                            'description' => __('Enables autoplay mode.', 'hemelios'),
                            'value' => array(__('Yes, please', 'hemelios') => 'yes')
                        ),
                        $add_el_class,
                        $add_css_animation,
                        $add_duration_animation,
                        $add_delay_animation
                    )
                ));
                //Counter Shortcode
                vc_map(array(
                    'name' => __('Counter', 'hemelios'),
                    'base' => 'os_counter',
                    'class' => '',
                    'icon' => 'fa fa-tachometer',
                    'category' => G5PLUS_FRAMEWORK_SHORTCODE_CATEGORY,
                    'params' => array(
                        array(
                            'type' => 'dropdown',
                            'heading' => __('Layout Style', 'hemelios'),
                            'param_name' => 'layout_style',
                            'admin_label' => true,
                            'value' => array(__('style 1', 'hemelios') => 'style1'),
                            'description' => __('Select Layout Style.', 'hemelios')
                        ),
                        array(
                            'type' => 'textfield',
                            'heading' => __('Title', 'hemelios'),
                            'param_name' => 'title',
                            'admin_label' => true,
                            'value' => '',
                        ),
                        array(
                            'type' => 'textfield',
                            'heading' => __('Value', 'hemelios'),
                            'param_name' => 'value',
                            'value' => '',
                        ),

                        array(
                            'type' => 'colorpicker',
                            'heading' => __('Text Color', 'hemelios'),
                            'param_name' => 'text_color',
                            'value' => '#444444',
                        ),

                        array(
                            'type' => 'textfield',
                            'heading' => __('Padding top', 'hemelios'),
                            'param_name' => 'padding_top',
                            'value' => '97',
                        ),
                        array(
                            'type' => 'textfield',
                            'heading' => __('Padding bottom', 'hemelios'),
                            'param_name' => 'padding_bottom',
                            'value' => '115',
                        ),
                        $add_el_class
                    )
                ));

                //Support Shortcode
                vc_map(array(
                    'name' => __('Support', 'hemelios'),
                    'base' => 'os_support',
                    'class' => '',
                    'icon' => 'fa fa-question-circle',
                    'category' => G5PLUS_FRAMEWORK_SHORTCODE_CATEGORY,
                    'params' => array(
                        array(
                            'type' => 'dropdown',
                            'heading' => __('Layout Style', 'hemelios'),
                            'param_name' => 'layout_style',
                            'admin_label' => true,
                            'value' => array(
                                __('style 1', 'hemelios') => 'style1',
                                __('style 2', 'hemelios') => 'style2'
                            ),
                            'description' => __('Select Layout Style.', 'hemelios')
                        ),
                        array(
                            'type' => 'textfield',
                            'heading' => __('Name', 'hemelios'),
                            'param_name' => 'name',
                            'admin_label' => true,
                            'value' => '',
                        ),
                        array(
                            'type' => 'textfield',
                            'heading' => __('Description', 'hemelios'),
                            'param_name' => 'description',
                            'value' => '',
                        ),
                        array(
                            'type' => 'attach_image',
                            'heading' => __('Avata', 'hemelios'),
                            'param_name' => 'avata',
                            'value' => '',
                        ),
                        array(
                            'type' => 'textfield',
                            'heading' => __('Skype', 'hemelios'),
                            'param_name' => 'skype',
                            'value' => '',
                        ),
                        array(
                            'type' => 'textfield',
                            'heading' => __('Email', 'hemelios'),
                            'param_name' => 'email',
                            'value' => '',
                        ),
                        $add_el_class
                    )
                ));


                //Piechart Shortcode
                vc_map(array(
                    'name' => __('Pie chart', 'hemelios'),
                    'base' => 'vc_pie',
                    'class' => '',
                    'icon' => 'icon-wpb-vc_pie',
                    "category" => G5PLUS_FRAMEWORK_SHORTCODE_CATEGORY,
                    'description' => __('Animated pie chart', 'hemelios'),
                    'params' => array(
                        array(
                            'type' => 'dropdown',
                            'heading' => __('Layout Style', 'hemelios'),
                            'param_name' => 'layout_style',
                            'admin_label' => true,
                            'value' => array(__('style 1', 'hemelios') => 'style1', __('style 2', 'hemelios') => 'style2'),
                            'description' => __('Select Layout Style.', 'hemelios')
                        ),
                        array(
                            'type' => 'textfield',
                            'heading' => __('Pie value', 'hemelios'),
                            'param_name' => 'value',
                            'description' => __('Input graph value here. Choose range between 0 and 100.', 'hemelios'),
                            'value' => '50',
                            'admin_label' => true
                        ),
                        array(
                            'type' => 'textfield',
                            'heading' => __('Pie label value', 'hemelios'),
                            'param_name' => 'label_value',
                            'description' => __('Input integer value for label. If empty "Pie value" will be used.', 'hemelios'),
                            'value' => ''
                        ),
                        array(
                            'type' => 'textfield',
                            'heading' => __('Units', 'hemelios'),
                            'param_name' => 'units',
                            'description' => __('Enter measurement units (if needed) Eg. %, px, points, etc. Graph value and unit will be appended to the graph title.', 'hemelios')
                        ),
                        array(
                            'type' => 'dropdown',
                            'heading' => __('Bar color', 'hemelios'),
                            'param_name' => 'color',
                            'value' => $colors_arr, //$pie_colors,
                            'description' => __('Select pie chart color.', 'hemelios'),
                            'admin_label' => true,
                            'param_holder_class' => 'vc_colored-dropdown'
                        ),
                        array(
                            'type' => 'textfield',
                            'heading' => __('Title', 'hemelios'),
                            'param_name' => 'title',
                            'value' => ''
                        ),
                        $add_el_class
                    )
                ));
                //Portfolio
                if (!isset($cpt_disable) || $cpt_disable['portfolio'] == '0' || $cpt_disable['portfolio'] == '') {
                    $portfolio_categories = get_terms(G5PLUS_PORTFOLIO_CATEGORY_TAXONOMY, array('hide_empty' => 0, 'orderby' => 'ASC'));
                    $portfolio_cat = array();
                    if (is_array($portfolio_categories)) {
                        foreach ($portfolio_categories as $cat) {
                            $portfolio_cat[$cat->name] = $cat->term_id;
                        }
                    }
                    vc_map(array(
                        'name' => __('Portfolio', 'hemelios'),
                        'base' => 'g5plusframework_portfolio',
                        'class' => '',
                        'icon' => 'fa fa-th-large',
                        'category' => G5PLUS_FRAMEWORK_SHORTCODE_CATEGORY,
                        'params' => array(
                            array(
                                'type' => 'dropdown',
                                'heading' => __('Style', 'hemelios'),
                                'param_name' => 'style',
                                'admin_label' => true,
                                'value' => array(__('Dark', 'hemelios') => '',
                                    __('Light', 'hemelios') => 'style_2')
                            ),
                            array(
                                'type' => 'textfield',
                                'heading' => __('Title', 'hemelios'),
                                'param_name' => 'title',
                                'value' => ''
                            ),
                            array(
                                'type' => 'dropdown',
                                'heading' => __('Layout Style', 'hemelios'),
                                'param_name' => 'show_pagging',
                                'value' => array(
                                    __('Grid', 'hemelios') => '1',
                                    __('Slider', 'hemelios') => '2')
                            ),
                            array(
                                'type' => 'multi-select',
                                'heading' => __('Portfolio Category', 'hemelios'),
                                'param_name' => 'category',
                                'options' => $portfolio_cat
                            ),
                            array(
                                'type' => 'checkbox',
                                'heading' => __('Show Category', 'hemelios'),
                                'param_name' => 'show_category',
                                'admin_label' => true,
                                'value' => array(__('Yes, please', 'hemelios') => 'yes')
                            ),
                            array(

                                'type' => 'dropdown',
                                'heading' => __('Category Style', 'hemelios'),
                                'param_name' => 'category_style',
                                'admin_label' => true,
                                'value' => array(__('Normal', 'hemelios') => 'cat-style-normal',
                                    __('Has background', 'hemelios') => 'background-cat')
                            ),
                            array(
                                'type' => 'dropdown',
                                'heading' => __('Number of column', 'hemelios'),
                                'param_name' => 'column',
                                'value' => array('2' => '2',
                                    '3' => '3',
                                    '4' => '4')
                            ),
                            array(
                                'type' => 'textfield',
                                'heading' => __('Number of item (or number of item per page if choose show paging)', 'hemelios'),
                                'param_name' => 'item',
                                'value' => ''
                            ),
                            array(
                                'type' => 'dropdown',
                                'heading' => __('Order Post Date By', 'hemelios'),
                                'param_name' => 'order',
                                'value' => array(__('Descending', 'hemelios') => 'DESC', __('Ascending', 'hemelios') => 'ASC')
                            ),
                            array(
                                'type' => 'dropdown',
                                'heading' => __('Padding', 'hemelios'),
                                'param_name' => 'padding',
                                'value' => array(__('15px', 'hemelios') => 'col-padding-15', __('No padding', 'hemelios') => 'col-no-padding')
                            ),

                            $add_el_class,
                            $add_css_animation,
                            $add_duration_animation,
                            $add_delay_animation

                        )
                    ));
                }
                //Service
                if (!isset($cpt_disable) || $cpt_disable['services'] == '0' || $cpt_disable['services'] == '') {
                    $service_categories = get_terms(G5PLUS_SERVICE_CATEGORY_TAXONOMY, array('hide_empty' => 0, 'orderby' => 'ASC'));
                    $service_cat = array();
                    if (is_array($service_categories)) {
                        foreach ($service_categories as $cat) {
                            $service_cat[$cat->name] = $cat->term_id;
                        }
                    }
                    vc_map(array(
                        'name' => __('Services', 'hemelios'),
                        'base' => 'g5plusframework_services',
                        'class' => '',
                        'icon' => 'fa fa-th-large',
                        'category' => G5PLUS_FRAMEWORK_SHORTCODE_CATEGORY,
                        'params' => array(
                            array(
                                'type' => 'dropdown',
                                'heading' => __('Layout Style', 'hemelios'),
                                'param_name' => 'style',
                                'admin_label' => true,
                                'value' => array(__('Style 1', 'hemelios') => 'style_1',
                                    __('Style 2', 'hemelios') => 'style_2'
                                )
                            ),
                            array(
                                'type' => 'textfield',
                                'heading' => __('Title', 'hemelios'),
                                'param_name' => 'title',
                                'value' => ''
                            ),
                            $title_style,
                            array(
                                'type' => 'dropdown',
                                'heading' => __('Color Style', 'hemelios'),
                                'param_name' => 'style_header',
                                'admin_label' => true,
                                'value' => array(__('light', 'hemelios') => 'light',
                                    __('dark', 'hemelios') => 'dark')
                            ),
                            array(
                                'type' => 'multi-select',
                                'heading' => __('Portfolio Category', 'hemelios'),
                                'param_name' => 'category',
                                'options' => $service_cat
                            ),
                            array(
                                'type' => 'dropdown',
                                'heading' => __('Number of column', 'hemelios'),
                                'param_name' => 'column',
                                'value' => array('2' => '2', '3' => '3', '4' => '4')
                            ),
                            array(
                                'type' => 'textfield',
                                'heading' => __('Number of item ', 'hemelios'),
                                'param_name' => 'item',
                                'value' => ''
                            ),
                            array(
                                'type' => 'dropdown',
                                'heading' => __('Order Post Date By', 'hemelios'),
                                'param_name' => 'order',
                                'value' => array(__('Descending', 'hemelios') => 'DESC', __('Ascending', 'hemelios') => 'ASC')
                            ),
                            array(
                                'type' => 'checkbox',
                                'heading' => __('Show Read More button', 'hemelios'),
                                'param_name' => 'show_readmore',
                                'admin_label' => false,
                                'value' => array(__('Yes, please', 'hemelios') => 'yes')
                            ),
                            $add_el_class,
                            $add_css_animation,
                            $add_duration_animation,
                            $add_delay_animation
                        )
                    ));
                }
                // Our Team
                if (!isset($cpt_disable) || $cpt_disable['ourteam'] == '0' || $cpt_disable['ourteam'] == '') {
                    $ourteam_cat = array();
                    $ourteam_categories = get_terms('ourteam_category', array('hide_empty' => 0, 'orderby' => 'ASC'));
                    if (is_array($ourteam_categories)) {
                        foreach ($ourteam_categories as $cat) {
                            $ourteam_cat[$cat->name] = $cat->slug;
                        }
                    }
                    vc_map(array(
                        'name' => __('Our Team', 'hemelios'),
                        'base' => 'os_ourteam',
                        'class' => '',
                        'icon' => 'fa fa-users',
                        'category' => G5PLUS_FRAMEWORK_SHORTCODE_CATEGORY,
                        'params' => array(
                            array(
                                'type' => 'dropdown',
                                'heading' => __('Layout Style', 'hemelios'),
                                'param_name' => 'layout_style',
                                'admin_label' => true,
                                'value' => array(__('style 1', 'hemelios') => 'style1',
                                    __('style 2', 'hemelios') => 'style2'),
                                'description' => __('Select Layout Style.', 'hemelios')
                            ),
                            array(
                                'type' => 'textfield',
                                'heading' => __('Item Amount', 'hemelios'),
                                'param_name' => 'item_amount',
                                'value' => '8'
                            ),
                            array(
                                'type' => 'textfield',
                                'heading' => __('Column', 'hemelios'),
                                'param_name' => 'column',
                                'value' => '4'
                            ),
                            array(
                                'type' => 'checkbox',
                                'heading' => __('Slider Style', 'hemelios'),
                                'param_name' => 'is_slider',
                                'admin_label' => false,
                                'value' => array(__('Yes, please', 'hemelios') => 'yes')
                            ),
                            array(
                                'type' => 'multi-select',
                                'heading' => __('Category', 'hemelios'),
                                'param_name' => 'category',
                                'options' => $ourteam_cat
                            ),
                            $add_el_class,
                            $add_css_animation,
                            $add_duration_animation,
                            $add_delay_animation
                        )
                    ));
                }
                // Button
                vc_map(array(
                    'name' => __('Button', 'hemelios'),
                    'base' => 'os_button',
                    'class' => '',
                    'icon' => 'fa fa-bold',
                    'category' => G5PLUS_FRAMEWORK_SHORTCODE_CATEGORY,
                    'params' => array(
                        array(
                            'type' => 'dropdown',
                            'heading' => __('Layout Style', 'hemelios'),
                            'param_name' => 'layout_style',
                            'admin_label' => true,
                            'value' => array(
                                __('Style 1', 'hemelios') => 'style1',
                                __('Style 2', 'hemelios') => 'style2',
                                __('Style 3', 'hemelios') => 'style3',
                                __('Style 4', 'hemelios') => 'style4'
                            ),
                            'description' => __('Select button layout style.', 'hemelios')
                        ),
                        array(
                            'type' => 'vc_link',
                            'heading' => __('URL (Link)', 'hemelios'),
                            'param_name' => 'link',
                            'description' => __('Add link to button.', 'hemelios'),
                        ),
                        array(
                            'type' => 'dropdown',
                            'heading' => __('Size', 'hemelios'),
                            'param_name' => 'size',
                            'description' => __('Select button display size.', 'hemelios'),
                            'std' => 'md',
                            'value' => array(
                                __('Mini', 'hemelios') => 'xs',
                                __('Small', 'hemelios') => 'sm',
                                __('Medium', 'hemelios') => 'md',
                                __('Large', 'hemelios') => 'lg',
                            )
                        ),
                        array(
                            'type' => 'checkbox',
                            'heading' => __('Use icon?', 'hemelios'),
                            'param_name' => 'add_icon',
                        ),
                        array(
                            'type' => 'icon_text',
                            'heading' => __('Select Icon', 'hemelios'),
                            'param_name' => 'icon',
                            'value' => '',
                            'description' => __('Select the icon in the popup window.', 'hemelios'),
                            'dependency' => array(
                                'element' => 'add_icon',
                                'value' => 'true',
                            ),
                        ),
                        array(
                            'type' => 'dropdown',
                            'heading' => __('Icon Alignment', 'hemelios'),
                            'description' => __('Select icon alignment.', 'hemelios'),
                            'param_name' => 'i_align',
                            'value' => array(
                                __('Left', 'hemelios') => 'left',
                                __('Right', 'hemelios') => 'right',
                            ),
                            'dependency' => array(
                                'element' => 'add_icon',
                                'value' => 'true',
                            ),
                        ),
                        $add_el_class,
                        $add_css_animation,
                        $add_duration_animation,
                        $add_delay_animation
                    )
                ));
                // Feature Box
                vc_map(array(
                    'name' => __('Feature Box', 'hemelios'),
                    'base' => 'os_feature',
                    'class' => '',
                    'icon' => 'fa fa-th-list',
                    'category' => G5PLUS_FRAMEWORK_SHORTCODE_CATEGORY,
                    'params' => array(
                        array(
                            'type' => 'dropdown',
                            'heading' => __('Layout Style', 'hemelios'),
                            'param_name' => 'layout_style',
                            'admin_label' => true,
                            'value' => array(__('style 1', 'hemelios') => 'style1'),
                            'description' => __('Select Layout Style.', 'hemelios')
                        ),
                        array(
                            'type' => 'attach_image',
                            'heading' => __('Image:', 'hemelios'),
                            'param_name' => 'image',
                            'value' => '',
                        ),
                        array(
                            'type' => 'textfield',
                            'heading' => __('Video Url', 'hemelios'),
                            'param_name' => 'video_url',
                            'value' => '',
                        ),
                        array(
                            'type' => 'textfield',
                            'heading' => __('Title', 'hemelios'),
                            'param_name' => 'title',
                            'value' => '',
                        ),
                        $title_style,
                        array(
                            'type' => 'icon_text',
                            'heading' => __('Select Icon:', 'hemelios'),
                            'param_name' => 'icon',
                            'value' => '',
                            'description' => __('Select the icon from the list.', 'hemelios'),
                        ),
                        array(
                            'type' => 'textarea',
                            'heading' => __('Description', 'hemelios'),
                            'param_name' => 'description',
                            'value' => '',
                        ),
                        array(
                            'type' => 'vc_link',
                            'heading' => __('Link (url)', 'hemelios'),
                            'param_name' => 'link',
                            'value' => '',
                        ),
                        $add_el_class,
                        $add_css_animation,
                        $add_duration_animation,
                        $add_delay_animation
                    )
                ));
                // Post
                vc_map(array(
                    'name' => __('Posts', 'hemelios'),
                    'base' => 'os_post',
                    'icon' => 'fa fa-file-text-o',
                    'category' => G5PLUS_FRAMEWORK_SHORTCODE_CATEGORY,
                    'description' => __('Posts', 'hemelios'),
                    'params' => array(
                        array(
                            'type' => 'dropdown',
                            'heading' => __('Post Style', 'hemelios'),
                            'param_name' => 'post_style',
                            'value' => array(
                                __('Style 1', 'hemelios') => 'style1',
                                __('Style 2', 'hemelios') => 'style2'
                            ),
                            'description' => 'choose post style'
                        ),
                        array(
                            'type' => 'textfield',
                            'heading' => __('Title', 'hemelios'),
                            'param_name' => 'title',
                            'value' => '',
                        ),
                        $title_style,
                        array(
                            'type' => 'dropdown',
                            'heading' => __('Display', 'hemelios'),
                            'param_name' => 'display',
                            'admin_label' => true,
                            'value' => array(__('Random', '') => 'random', __('Popular', 'hemelios') => 'popular', __('Recent', 'hemelios') => 'recent', __('Oldest', 'hemelios') => 'oldest'),
                            'description' => __('Select Orderby.', 'hemelios'),
                            'std' => 'recent'
                        ),
                        array(
                            'type' => 'textfield',
                            'heading' => __('Item Amount', 'hemelios'),
                            'param_name' => 'item_amount',
                            'value' => '10'
                        ),
                        array(
                            'type' => 'textfield',
                            'heading' => __('Column', 'hemelios'),
                            'param_name' => 'column',
                            'value' => '3'
                        ),
                        array(
                            'type' => 'checkbox',
                            'heading' => __('Slider Style', 'hemelios'),
                            'param_name' => 'is_slider',
                            'admin_label' => false,
                            'value' => array(__('Yes, please', 'hemelios') => 'yes')
                        ),
                        $add_el_class,
                        $add_css_animation,
                        $add_duration_animation,
                        $add_delay_animation
                    )
                ));
                // Client
                vc_map(array(
                    'name' => __('Client', 'hemelios'),
                    'base' => 'os_partner_carousel',
                    'icon' => 'fa fa-user-plus',
                    'category' => G5PLUS_FRAMEWORK_SHORTCODE_CATEGORY,
                    'description' => __('Animated carousel with images', 'hemelios'),
                    'params' => array(
                        array(
                            'type' => 'dropdown',
                            'heading' => __('Layout Style', 'hemelios'),
                            'param_name' => 'layout_style',
                            'admin_label' => true,
                            'value' => array(__('Grid Layout', 'hemelios') => 'style1',
                                __('Slider Layout', 'hemelios') => 'style2'),
                            'description' => __('Select Layout Style.', 'hemelios')
                        ),
                        array(
                            'type' => 'textfield',
                            'heading' => __('Title', 'hemelios'),
                            'param_name' => 'title',
                        ),
                        $title_style,
                        array(
                            'type' => 'attach_images',
                            'heading' => __('Images', 'hemelios'),
                            'param_name' => 'images',
                            'value' => '',
                            'description' => __('Select images from media library.', 'hemelios')
                        ),
                        array(
                            'type' => 'textfield',
                            'heading' => __('Image size', 'hemelios'),
                            'param_name' => 'img_size',
                            'value' => 'full',
                            'description' => __('Enter image size. Example: thumbnail, medium, large, full or other sizes defined by current theme. Alternatively enter image size in pixels: 200x100 (Width x Height). Leave empty to use "thumbnail" size.', 'hemelios')
                        ),
                        array(
                            'type' => 'dropdown',
                            'heading' => __('Image Opacity', 'hemelios'),
                            'param_name' => 'opacity',
                            'value' => array(
                                __('10%', 'hemelios') => '10',
                                __('20%', 'hemelios') => '20',
                                __('30%', 'hemelios') => '30',
                                __('40%', 'hemelios') => '40',
                                __('50%', 'hemelios') => '50',
                                __('60%', 'hemelios') => '60',
                                __('70%', 'hemelios') => '70',
                                __('80%', 'hemelios') => '80',
                                __('90%', 'hemelios') => '90',
                                __('100%', 'hemelios') => '100'
                            ),
                            'std' => '80'
                        ),
                        array(
                            'type' => 'exploded_textarea',
                            'heading' => __('Custom links', 'hemelios'),
                            'param_name' => 'custom_links',
                            'description' => __('Enter links for each slide here. Divide links with linebreaks (Enter) . ', 'hemelios'),
//							'dependency'  => array(
//								'element' => 'onclick',
//								'value'   => array( 'custom_link' )
//							)
                        ),
                        array(
                            'type' => 'dropdown',
                            'heading' => __('Custom link target', 'hemelios'),
                            'param_name' => 'custom_links_target',
                            'description' => __('Select where to open  custom links.', 'hemelios'),
//							'dependency'  => array(
//								'element' => 'onclick',
//								'value'   => array( 'custom_link' )
//							),
                            'value' => $target_arr
                        ),
                        array(
                            'type' => 'dropdown',
                            'heading' => __('Client per view', 'hemelios'),
                            'param_name' => 'column',
                            'admin_label' => true,
                            'value' => array(
                                __('1', 'hemelios') => '1',
                                __('2', 'hemelios') => '2',
                                __('3', 'hemelios') => '3',
                                __('4', 'hemelios') => '4',
                                __('5', 'hemelios') => '5',
                                __('6', 'hemelios') => '6',
                            ),
                            'description' => __('Set number of content per row.', 'hemelios')
                        ),
                        array(
                            'type' => 'checkbox',
                            'heading' => __('Slider autoplay', 'hemelios'),
                            'param_name' => 'autoplay',
                            'description' => __('Enables autoplay mode.', 'hemelios'),
                            'value' => array(__('Yes, please', 'hemelios') => 'yes'),
                            'dependency' => array('element' => 'layout_style', 'value' => array('style2')),
                        ),
                        array(
                            'type' => 'checkbox',
                            'heading' => __('Show pagination control', 'hemelios'),
                            'param_name' => 'pagination',
                            'value' => array(__('Yes, please', 'hemelios') => 'yes'),
                            'dependency' => array('element' => 'layout_style', 'value' => array('style2')),
                        ),
                        array(
                            'type' => 'checkbox',
                            'heading' => __('Show navigation control', 'hemelios'),
                            'param_name' => 'navigation',
                            'value' => array(__('Yes, please', 'hemelios') => 'yes'),
                            'dependency' => array('element' => 'layout_style', 'value' => array('style2')),
                        ),
                        $add_el_class,
                        $add_css_animation,
                        $add_duration_animation,
                        $add_delay_animation
                    )
                ));
                // Heading
                vc_map(array(
                    'name' => __('Headings', 'hemelios'),
                    'base' => 'os_heading',
                    'icon' => 'fa fa-header',
                    'category' => G5PLUS_FRAMEWORK_SHORTCODE_CATEGORY,
                    'params' => array(
                        array(
                            'type' => 'dropdown',
                            'heading' => __('Layout Style', 'hemelios'),
                            'param_name' => 'layout_style',
                            'admin_label' => true,
                            'value' => array(__('Dark', 'hemelios') => 'style1',
                                __('Light', 'hemelios') => 'style2'),
                            'description' => __('Select Layout Style.', 'hemelios')
                        ),
                        array(
                            'type' => 'textfield',
                            'heading' => __('Title', 'hemelios'),
                            'param_name' => 'title',
                            'value' => '',
                            'admin_label' => true
                        ),
                        $title_style,
                        array(
                            'type' => 'textfield',
                            'heading' => __('Font sie Title (no unit)', 'hemelios'),
                            'param_name' => 'font_size_title',
                            'value' => '20',
                            'admin_label' => true
                        ),
                        array(
                            'type' => 'dropdown',
                            'heading' => __('Heading Align', 'hemelios'),
                            'param_name' => 'heading_align',
                            'value' => array(
                                __('Left', 'hemelios') => 'left',
                                __('Center', 'hemelios') => 'center',
                                __('right', 'hemelios') => 'Right'
                            ),
                        ),
                        $add_el_class,
                        $add_css_animation,
                        $add_duration_animation,
                        $add_delay_animation
                    )
                ));

                // Heading
                vc_map(array(
                    'name' => __('Icon Home', 'hemelios'),
                    'base' => 'os_icon_home',
                    'icon' => 'fa fa-header',
                    'category' => G5PLUS_FRAMEWORK_SHORTCODE_CATEGORY,
                    'params' => array(

                    )
                ));


                // Notification
                vc_map(array(
                    'name' => __('Notifications', 'hemelios'),
                    'base' => 'os_notification',
                    'icon' => 'fa fa-exclamation-triangle',
                    'category' => G5PLUS_FRAMEWORK_SHORTCODE_CATEGORY,
                    'params' => array(
                        array(
                            'type' => 'dropdown',
                            'heading' => __('Layout Style', 'hemelios'),
                            'param_name' => 'layout_style',
                            'admin_label' => true,
                            'value' => array(__('Small style', 'hemelios') => 'style1',
                                __('Larger style', 'hemelios') => 'style2'),
                            'description' => __('Select Layout Style.', 'hemelios')
                        ),
                        array(
                            'type' => 'dropdown',
                            'heading' => __('Message type', 'hemelios'),
                            'param_name' => 'message_type',
                            'admin_label' => true,
                            'value' => array(__('Notice message', 'hemelios') => 'type-1',
                                __('Error message', 'hemelios') => 'type-2',
                                __('Warning message', 'hemelios') => 'type-3',
                                __('Success message', 'hemelios') => 'type-4',
                                __('Info message', 'hemelios') => 'type-5'),
                            'description' => __('Select Message type.', 'hemelios'),
                            'admin_label' => true
                        ),
                        array(
                            'type' => 'textfield',
                            'heading' => __('Title', 'hemelios'),
                            'param_name' => 'title',
                            'value' => '',
                            'dependency' => array('element' => 'layout_style', 'value' => array('style2')),
                        ),
                        array(
                            'type' => 'textfield',
                            'heading' => __('Descriptions', 'hemelios'),
                            'param_name' => 'description',
                            'value' => '',
                        ),
                        $add_el_class,
                        $add_css_animation,
                        $add_duration_animation,
                        $add_delay_animation
                    )
                ));
                // Call To Action
                vc_map(array(
                    'name' => __('Call To Action', 'hemelios'),
                    'base' => 'os_call_to_action',
                    'icon' => 'fa fa-paper-plane-o',
                    'category' => G5PLUS_FRAMEWORK_SHORTCODE_CATEGORY,
                    'params' => array(
                        array(
                            'type' => 'textfield',
                            'heading' => __('Title', 'hemelios'),
                            'param_name' => 'title',
                            'value' => 'For expert financial advice you can trust Hemelios',
                        ),
                        array(
                            'type' => 'vc_link',
                            'heading' => __('Call to action link', 'hemelios'),
                            'param_name' => 'link',
                        ),
                        array(
                            'type' => 'checkbox',
                            'heading' => __('Use custom color', 'hemelios'),
                            'param_name' => 'custom_color',
                            'value' => array(__('Yes, please', 'hemelios') => 'yes'),
                            'description' => 'Not use default color'
                        ),
                        array(
                            'type' => 'colorpicker',
                            'heading' => __('Text Color', 'hemelios'),
                            'param_name' => 'color',
                            'value' => '',
                            'dependency' => array(
                                'element' => 'custom_color',
                                'value' => 'yes'
                            )
                        ),
                        array(
                            'type' => 'colorpicker',
                            'heading' => __('Background Color', 'hemelios'),
                            'param_name' => 'bg_color',
                            'value' => '',
                            'dependency' => array(
                                'element' => 'custom_color',
                                'value' => 'yes'
                            )
                        ),
                        $add_el_class,
                        $add_css_animation,
                        $add_duration_animation,
                        $add_delay_animation
                    )
                ));
                // Icon box
                vc_map(
                    array(
                        'name' => __('Icon Box', 'hemelios'),
                        'base' => 'os_icon_box',
                        'icon' => 'fa fa-diamond',
                        'category' => G5PLUS_FRAMEWORK_SHORTCODE_CATEGORY,
                        'description' => 'Adds icon box with font icons',
                        'params' => array(
                            array(
                                'type' => 'dropdown',
                                'heading' => __('Layout Type', 'hemelios'),
                                'param_name' => 'layout_type',
                                'admin_label' => true,
                                'value' => array(
                                    __('Style 1', 'hemelios') => 'style1',
                                    __('Style 2', 'hemelios') => 'style2',
                                    __('Style 3', 'hemelios') => 'style3'),
                                'description' => __('Select Layout Type.', 'hemelios')
                            ),
                            array(
                                'type' => 'dropdown',
                                'heading' => __('Color Style', 'hemelios'),
                                'param_name' => 'color_style',
                                'admin_label' => true,
                                'value' => array(__('Inherit', 'hemelios') => 'istyle-inherit',
                                                 __('Dark', 'hemelios') => 'istyle-dark',
                                                 __('Light', 'hemelios') => 'istyle-light'),
                                'description' => __('Select Color Style.', 'hemelios')
                            ),
                            array(
                                'type' => 'colorpicker',
                                'heading' => __('Background Color', 'hemelios'),
                                'param_name' => 'bg_color',
                                'admin_label' => true,
                            ),

                            array(
                                'type' => 'dropdown',
                                'heading' => __('Icon to display:', 'hemelios'),
                                'param_name' => 'icon_type',
                                'value' => array(
                                    __('Font Icon', 'hemelios') => 'font-icon',
                                    __('Custom Image Icon', 'hemelios') => 'custom',
                                ),
                                'description' => __('Select which icon you would like to use', 'hemelios')
                            ),
                            array(
                                'type' => 'icon_text',
                                'heading' => __('Select Icon', 'hemelios'),
                                'param_name' => 'icon',
                                'value' => '',
                                'description' => __('Select the icon in the popup window.', 'hemelios'),
                                'dependency' => array('element' => 'icon_type', 'value' => array('font-icon')),
                            ),
                            array(
                                'type' => 'dropdown',
                                'heading' => __('Select Icon Stype', 'hemelios'),
                                'param_name' => 'icon_style',
                                'value' => array(
                                    __('Simple Icon', 'hemelios') => 'simple-icon',
                                    __('Circle Icon', 'hemelios') => 'circle-icon',
                                ),
                                'description' => __('Set Icon Style.', 'hemelios'),
                                'dependency' => array('element' => 'icon_type', 'value' => array('font-icon')),
                            ),

                            // Play with icon selector
                            array(
                                'type' => 'attach_image',
                                'heading' => __('Upload Image Icon:', 'hemelios'),
                                'param_name' => 'image',
                                'value' => '',
                                'description' => __('Upload the custom image icon.', 'hemelios'),
                                'dependency' => array('element' => 'icon_type', 'value' => array('custom')),
                            ),
                            array(
                                'type' => 'vc_link',
                                'heading' => __('Link (url)', 'hemelios'),
                                'param_name' => 'link',
                                'value' => '',
                            ),
                            array(
                                'type' => 'textfield',
                                'heading' => __('Title', 'hemelios'),
                                'param_name' => 'title',
                                'value' => '',
                                'description' => __('Provide the title for this icon box.', 'hemelios'),
                            ),
                            $title_style,
                            array(
                                'type' => 'textarea',
                                'heading' => __('Description', 'hemelios'),
                                'param_name' => 'description',
                                'value' => '',
                                'description' => __('Provide the description for this icon box.', 'hemelios'),
                            ),
                            array(
                                'type' => 'textfield',
                                'heading' => __('Symbol Background', 'hemelios'),
                                'param_name' => 'symbol_bg',
                                'value' => '',
                                'description' => __('Symbol background should not more than 2 character', 'hemelios'),
                            ),
                            $add_el_class,
                            $add_css_animation,
                            $add_duration_animation,
                            $add_delay_animation
                        )
                    )
                );
                // Gallery
                vc_map(
                    array(
                        'name' => __('Gallery', 'hemelios'),
                        'base' => 'os_gallery',
                        'icon' => 'fa fa-file-image-o',
                        'category' => G5PLUS_FRAMEWORK_SHORTCODE_CATEGORY,
                        'params' => array(
                            array(
                                'type' => 'dropdown',
                                'heading' => __('Layout Style', 'hemelios'),
                                'param_name' => 'layout_style',
                                'admin_label' => true,
                                'value' => array(__('Has padding', 'hemelios') => 'style1',
                                    __('No Padding', 'hemelios') => 'style2'),
                                'description' => __('Select Layout Style.', 'hemelios')
                            ),
                            array(
                                'type' => 'attach_images',
                                'heading' => __('Select Images:', 'hemelios'),
                                'param_name' => 'images_gallery',
                                'description' => __('Upload images to gallery.', 'hemelios'),
                            ),
                            // Play with icon selector
                            array(
                                'type' => 'dropdown',
                                'heading' => __('Column number:', 'hemelios'),
                                'param_name' => 'column_number',
                                'admin_label' => true,
                                'value' => array(__('3', 'hemelios') => '3',
                                    __('4', 'hemelios') => '4',
                                    __('5', 'hemelios') => '5'),
                                'description' => __('Select Column number.', 'hemelios')
                            ),

                            $add_el_class,
                            $add_css_animation,
                            $add_duration_animation,
                            $add_delay_animation
                        )
                    )
                );

                //Timeline

                vc_map( array(
                    'name'     => __( 'TimeLine', 'viettitan' ),
                    'base'     => 'viettitan_timeline',
                    'class'    => '',
                    'icon'     => 'fa fa-line-chart',
                    'category' => G5PLUS_FRAMEWORK_SHORTCODE_CATEGORY,
                    'params'   => array(
                        array(
                            'type'       => 'param_group',
                            'value'       => urlencode( json_encode( array(
                                array(
                                    'title' => esc_html__( 'Timeline Item 1', 'viettitan' ),
                                ),
                                array(
                                    'title' => esc_html__( 'Timeline Item 2', 'viettitan' ),
                                ),
                                array(
                                    'title' => esc_html__( 'Timeline Item 3', 'viettitan' ),
                                ),
                            ) ) ),
                            'heading'    => 'TimeLine Items',
                            'param_name' => 'timeline_items',
                            // Note params is mapped inside param-group:
                            'params'     => array(
                                array(
                                    'type'        => 'textfield',
                                    'heading'     => __( 'Title:', 'viettitan' ),
                                    'param_name'  => 'title',
                                    'description' => __( 'Enter Title.', 'viettitan' ),
                                    'admin_label' => true,
                                ),
                                array(
                                    'type'        => 'attach_image',
                                    'heading'     => __( 'Select Image:', 'viettitan' ),
                                    'param_name'  => 'images_gallery',
                                    'description' => __( 'Upload images to gallery.', 'viettitan' ),
                                ),
                                array(
                                    'type'        => 'textarea',
                                    'heading'     => __( 'Description', 'viettitan' ),
                                    'param_name'  => 'description',
                                    'description' => __( 'Enter Description.', 'viettitan' ),
                                ),

                                array(
                                    'type'        => 'vc_link',
                                    'heading'     => __( 'Link', 'viettitan' ),
                                    'param_name'  => 'link',
                                ),

                                // Play with icon selector
                            )
                        ),
                        $add_el_class,
                        $add_css_animation,
                        $add_duration_animation,
                        $add_delay_animation
                    )
                ) );


                // Download
                vc_map(
                    array(
                        'name' => __('Download', 'hemelios'),
                        'base' => 'os_download',
                        'icon' => 'fa fa-download',
                        'category' => G5PLUS_FRAMEWORK_SHORTCODE_CATEGORY,
                        'params' => array(
                            array(
                                'type' => 'textfield',
                                'heading' => __('Title', 'hemelios'),
                                'param_name' => 'title',
                                'description' => __('Enter title.', 'hemelios')
                            ),
                            $title_style,
                            array(
                                'type' => 'os_attachment',
                                'heading' => __('Choose download files', 'hemelios'),
                                'param_name' => 'file_download',
                                'value' => '',
                                'description' => 'Choose multiple file by hold Ctrl, wait few seconds after chose files <br/>
<b>Note:</b> Select document file type only.'
                            ),
                            $add_el_class,
                            $add_css_animation,
                            $add_duration_animation,
                            $add_delay_animation
                        )
                    )
                );
                // Jobs Listing
                if(class_exists('WP_Job_Manager')){
                    vc_map(
                        array(
                            'name' => __('Jobs Listing', 'hemelios'),
                            'base' => 'jobs',
                            'icon' => 'fa fa-user-plus',
                            'category' => G5PLUS_FRAMEWORK_SHORTCODE_CATEGORY,
                            'description' => __('This is shortcode of Jobs Listing Plugin')
                        )
                    );
                }
                $product_cat = array();
                if (class_exists('WooCommerce')) {
                    $args = array(
                        'number' => '',
                    );
                    $product_categories = get_terms('product_cat', $args);
                    if (is_array($product_categories)) {
                        foreach ($product_categories as $cat) {
                            $product_cat[$cat->name] = $cat->slug;
                        }
                    }
                    // Product
                    vc_map(
                        array(
                            'name' => __('Product', 'hemelios'),
                            'base' => 'os_product',
                            'icon' => 'fa fa-shopping-cart',
                            'category' => G5PLUS_FRAMEWORK_SHORTCODE_CATEGORY,
                            'params' => array(
                                array(
                                    "type" => "textfield",
                                    "heading" => __("Title", 'hemelios'),
                                    "param_name" => "title",
                                    "admin_label" => true,
                                    "value" => ''
                                ),
                                $title_style,
                                array(
                                    'type' => 'dropdown',
                                    'heading' => __('Select product source', 'hemelios'),
                                    'param_name' => 'source',
                                    'value' => array(__('From feature', 'hemelios') => 'feature',
                                        __('From category', 'hemelios') => 'category',
                                    ),
                                ),
                                array(
                                    'type' => 'dropdown',
                                    'heading' => __('Feature', 'hemelios'),
                                    'param_name' => 'filter',
                                    'value' => array(__('Sale Off', 'hemelios') => 'sale',
                                        __('New In', 'hemelios') => 'new-in',
                                        __('Featured', 'hemelios') => 'featured',
                                        __('Top rated', 'hemelios') => 'top-rated',
                                        __('Recent review', 'hemelios') => 'recent-review',
                                        __('Best Selling', 'hemelios') => 'best-selling'
                                    ),
                                    'dependency' => array('element' => 'source', 'value' => array('feature'))
                                ),
                                array(
                                    'type' => 'multi-select',
                                    'heading' => __('Category', 'hemelios'),
                                    'param_name' => 'category',
                                    'options' => $product_cat,
                                    'dependency' => array('element' => 'source', 'value' => array('category'))
                                ),
                                array(
                                    "type" => "textfield",
                                    "heading" => __("Per Page", 'hemelios'),
                                    "param_name" => "per_page",
                                    "admin_label" => true,
                                    "value" => '8',
                                    "description" => __('How much items per page to show', 'hemelios')
                                ),
                                array(
                                    "type" => "textfield",
                                    "heading" => __("Columns", 'hemelios'),
                                    "param_name" => "columns",
                                    "value" => '4',
                                    "description" => __("How much columns grid", 'hemelios'),
                                ),
                                array(
                                    'type' => 'dropdown',
                                    'heading' => __('Display Slider', 'hemelios'),
                                    'param_name' => 'slider',
                                    'value' => array(__('No', 'hemelios') => '', __('Yes', 'hemelios') => 'slider'),
                                ),


                                array(
                                    'type' => 'dropdown',
                                    'heading' => __('Order by', 'hemelios'),
                                    'param_name' => 'orderby',
                                    'value' => array(__('Date', 'hemelios') => 'date', __('ID', 'hemelios') => 'ID',
                                        __('Author', 'hemelios') => 'author', __('Modified', 'hemelios') => 'modified',
                                        __('Random', 'hemelios') => 'rand', __('Comment count', 'hemelios') => 'comment_count',
                                        __('Menu Order', 'hemelios') => 'menu_order'
                                    ),
                                    'description' => __('Select how to sort retrieved products.', 'hemelios'),
                                ),
                                array(
                                    'type' => 'dropdown',
                                    'heading' => __('Order way', 'hemelios'),
                                    'param_name' => 'order',
                                    'value' => array(__('Descending', 'hemelios') => 'DESC', __('Ascending', 'hemelios') => 'ASC'),
                                    'description' => __('Designates the ascending or descending order.', 'hemelios'),
                                ),
                                $add_el_class,
                                $add_css_animation,
                                $add_duration_animation,
                                $add_delay_animation
                            )
                        )
                    );
                    // Product Categories
                    vc_map(array(
                        'name' => __('Product Categories', 'hemelios'),
                        'base' => 'os_product_categories',
                        'class' => '',
                        'icon' => 'fa fa-cart-plus',
                        'category' => G5PLUS_FRAMEWORK_SHORTCODE_CATEGORY,
                        'params' => array(
                            array(
                                "type" => "textfield",
                                "heading" => __("Title", 'hemelios'),
                                "param_name" => "title",
                                "admin_label" => true,
                                "value" => ''
                            ),
                            $title_style,
                            array(
                                'type' => 'multi-select',
                                'heading' => __('Product Category', 'hemelios'),
                                'param_name' => 'category',
                                'options' => $product_cat
                            ),
                            array(
                                "type" => "textfield",
                                "heading" => __("Columns", 'hemelios'),
                                "param_name" => "columns",
                                "value" => '4',
                                "description" => __("How much columns grid", 'hemelios'),
                            ),

                            array(
                                'type' => 'dropdown',
                                'heading' => __('Slider', 'hemelios'),
                                'param_name' => 'slider',
                                'value' => array(__('No', 'hemelios') => '',
                                    __('Yes', 'hemelios') => 'slider'
                                ),
                            ),
                            array(
                                'type' => 'dropdown',
                                'heading' => __('Hide empty', 'hemelios'),
                                'param_name' => 'hide_empty',
                                'value' => array(__('No', 'hemelios') => '0',
                                    __('Yes', 'hemelios') => '1'
                                ),
                            ),
                            array(
                                'type' => 'dropdown',
                                'heading' => __('Order by', 'hemelios'),
                                'param_name' => 'orderby',
                                'value' => array(__('Date', 'hemelios') => 'date', __('ID', 'hemelios') => 'ID',
                                    __('Author', 'hemelios') => 'author', __('Modified', 'hemelios') => 'modified',
                                    __('Random', 'hemelios') => 'rand', __('Comment count', 'hemelios') => 'comment_count',
                                    __('Menu Order', 'hemelios') => 'menu_order'
                                ),
                                'description' => __('Select how to sort retrieved products.', 'hemelios')
                            ),
                            array(
                                'type' => 'dropdown',
                                'heading' => __('Order way', 'hemelios'),
                                'param_name' => 'order',
                                'value' => array(__('Descending', 'hemelios') => 'DESC', __('Ascending', 'hemelios') => 'ASC'),
                                'description' => __('Designates the ascending or descending orde.', 'hemelios')
                            ),
                            $add_el_class,
                            $add_css_animation,
                            $add_duration_animation,
                            $add_delay_animation
                        )
                    ));
                }

            }
        }

    }

    if (!function_exists('init_g5plus_framework_shortcodes')) {
        function init_g5plus_framework_shortcodes()
        {
            return g5plusFramework_Shortcodes::init();
        }

        init_g5plus_framework_shortcodes();
    }
}
