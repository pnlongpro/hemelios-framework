<?php
// don't load directly
if ( !defined( 'ABSPATH' ) ) {
	die( '-1' );
}
if ( !class_exists( 'ViettitanFramework_Shortcode_TimeLine' ) ) {
	class ViettitanFramework_Shortcode_TimeLine {
		function __construct() {
			add_shortcode( 'viettitan_timeline', array( $this, 'timeline_shortcode' ) );
		}

		function timeline_shortcode( $atts ) {
			/**
			 * Shortcode attributes
			 * @var $timeline_items
			 * @var $css_animation
			 * @var $duration
			 * @var $delay
			 */
			$timeline_items = $image = $title = $description = $link = $el_class = $css_animation = $duration = $delay = '';
			$atts           = vc_map_get_attributes( 'viettitan_timeline', $atts );
			extract( $atts );
			$viettitan_animation = ' ' . esc_attr( $el_class ) . g5plusFramework_Shortcodes::g5plus_get_css_animation( $css_animation );

			ob_start(); ?>
			<div class="viettitan-timeline<?php echo esc_attr( $viettitan_animation ) ?>" <?php echo g5plusFramework_Shortcodes::g5plus_get_style_animation( $duration, $delay ); ?>>
				<?php
				$timeline_items = (array) vc_param_group_parse_atts( $timeline_items );
				if ( $timeline_items ) :
					?>
					<div class="timeline-wrapper">
						<div class="spine animated"></div>
						<?php
						$key = 0;
						foreach ( $timeline_items as $value ) :
							$key ++;
							if ( $key % 2 == 0 ) {
								$class = 'timeline_element_right wpb_fadeInRight';
							} else {
								$class = 'timeline_element_left wpb_fadeInLeft';
							}
							$gallery_image = $value['images_gallery'];
							$thumbnail_url = '';
							$width         = 200;
							$height        = 150;
							if ( !empty( $gallery_image ) ) {

								$images_attr = wp_get_attachment_image_src( $gallery_image, "full" );
								if ( isset( $images_attr ) ) {
									$resize = matthewruddy_image_resize( $images_attr[0], $width, $height );
									if ( $resize != null ) {
										$thumbnail_url = $resize['url'];
									}
								}
							}
							$image_alt = get_the_title( $gallery_image );
							//parse link
							$link     = $value['link'];
							$link     = vc_build_link( $link );
							$a_title  = '';
							$a_target = '_self';
							$a_href   = '#';
							if ( strlen( $link['url'] ) > 0 ) {
								$a_href   = $link['url'];
								$a_target = strlen( $link['target'] ) > 0 ? $link['target'] : '_self';
							}
							?>
							<div class="timeline_element wpb_animate_when_almost_visible g5plus-css-animation <?php echo esc_attr( $class ) ?>">
								<div class="timeline_element_box">
									<div class="timeline_title">
										<h3>
											<a href="<?php echo esc_url( $a_href ) ?>" target="<?php echo $a_target ?>" title=" <?php echo $value['title'] ?>"><?php echo $value['title'] ?></a>
										</h3>
									</div>
									<div class="timeline-thumb">
										<img src="<?php echo esc_url( $thumbnail_url ) ?>" alt="<?php echo esc_html( $image_alt ) ?>">
										<a class="img_overlay" href="<?php echo esc_url( $a_href ) ?>" target="<?php echo $a_target ?>" title=" <?php echo $value['title'] ?>">
											<i class="fa fa-search"></i>
										</a>
									</div>
									<div class="timeline-description">
										<p><?php echo $value['description'] ?></p>
									</div>
								</div>
							</div>
							<?php
						endforeach;
						?>
					</div>
					<?php
				endif;
				?>
			</div>
			<?php
			$content = ob_get_clean();

			return $content;
		}
	}

	new ViettitanFramework_Shortcode_TimeLine();
}