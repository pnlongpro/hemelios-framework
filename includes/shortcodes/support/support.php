<?php
// don't load directly
if ( !defined( 'ABSPATH' ) ) {
	die( '-1' );
}
if ( !class_exists( 'g5plusFramework_ShortCode_Support' ) ) {
	class g5plusFramework_ShortCode_Support {
		function __construct() {
			add_shortcode( 'os_support', array( $this, 'support_shortcode' ) );
		}

		function support_shortcode( $atts ) {
			$name = $layout_style = $description = $avata = $skype = $email = $html = $el_class = $css = $style = '';
			extract( shortcode_atts( array(
				'layout_style' => 'style1',
				'name'         => '',
				'description'  => '',
				'avata'        => '',
				'skype'        => '',
				'email'        => '',
				'el_class'     => ''
			), $atts ) );
			$img_size = '160x160';
			ob_start(); ?>
			<div class="hemelios-support <?php echo esc_attr( $layout_style ) ?> <?php echo esc_attr( $el_class ) ?>">
				<?php
					$post_thumbnail = wpb_getImageBySize( array( 'attach_id' => $avata, 'thumb_size' => $img_size ) );

					if ( $post_thumbnail ) {
						$thumbnail = $post_thumbnail;
					} else {
						$post_thumbnail                   = array();
						$post_thumbnail['thumbnail']      = '<img src="' . vc_asset_url( 'vc/no_image.png' ) . '" />';
						$post_thumbnail['p_img_large'][0] = vc_asset_url( 'vc/no_image.png' );
					}
					$thumbnail = $post_thumbnail['thumbnail'];


				?>
				<div itemscope itemtype="http://schema.org/LocalBusiness" class="support-avata">
					<?php echo $thumbnail; ?>
				</div>
				<div itemscope itemtype="http://schema.org/LocalBusiness" class="support-entry-content">
					<span itemprop="name" class="name"><?php echo esc_html( $name ) ?></span>
					<div class="description">
						<?php echo wp_kses_post( $description ) ?>
					</div>
					<div class="social">
						<?php if ( $skype ) : ?>
							<a data-toggle="tooltip" data-placement="top" title="Skype Name: <?php echo $skype ?>" href="skype:<?php echo $skype ?>?chat"><i class="fa fa-skype"></i></a>
						<?php endif; ?>
						<?php if ( $email ) : ?>
							<a data-toggle="tooltip" data-placement="top" title="Email: <?php echo $email ?>" href="mailto:<?php echo $email ?>"><i class="fa fa-envelope"></i></a>
						<?php endif; ?>
					</div>
				</div>
			</div>
			<?php
			$content = ob_get_clean();

			return $content;
		}
	}

	new g5plusFramework_ShortCode_Support();
}