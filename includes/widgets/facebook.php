<?php
/**
 * Created by PhpStorm.
 * User: PhanLong
 * Date: 3/6/2016
 * Time: 5:14 AM
 */

class Hemelios_Widget_Facebook extends  G5Plus_Widget {
    public function __construct() {
        $this->widget_cssclass    = 'widget-facebook';
        $this->widget_description = __( "Facebook Page Plugin", 'hemelios' );
        $this->widget_id          = 'hemelios-facebook';
        $this->widget_name        = __( 'Hemelios - Facebook', 'hemelios' );
        $this->settings           = array(
            'title'  => array(
                'type'  => 'text',
                'std'   => '',
                'label' => __( 'Title', 'hemelios' )
            ),
            'url' => array(
                'type'  => 'text',
                'std'   => '',
                'label' => __( 'URL', 'hemelios' )
            ),
            'width' => array(
                'type'  => 'text',
                'std'   => '',
                'label' => __( 'Width', 'hemelios' )
            ),
            'height' => array(
                'type'  => 'text',
                'std'   => '',
                'label' => __( 'Height', 'hemelios' )
            ),
            'use_small_header' => array(
                'type'  => 'checkbox',
                'std'   => '',
                'label' => __( 'User Small Header', 'hemelios' )
            ),
            'adapt_to_container' => array(
                'type'  => 'checkbox',
                'std'   => '',
                'label' => __( 'Adapt to shortcode container width', 'hemelios' )
            ),
            'hide_cover' => array(
                'type'  => 'checkbox',
                'std'   => '',
                'label' => __( 'Hide the cover photo in the header', 'hemelios' )
            ),
            'show_face' => array(
                'type'  => 'checkbox',
                'std'   => '',
                'label' => __( 'Show profile photos when friends like this', 'hemelios' )
            ),
            'show_posts' => array(
                'type'  => 'checkbox',
                'std'   => '',
                'label' => __( 'Show posts from the Page\'s timeline', 'hemelios' )
            ),

        );
        parent::__construct();
    }
    function widget($args, $instance) {
        if ( $this->get_cached_widget( $args ) )
            return;
        extract( $args, EXTR_SKIP );

        $title = (!empty( $instance['title'] ) ) ? $instance['title'] : '';
        $title = apply_filters( 'widget_title', $title, $instance, $this->id_base );
        $class_custom   = empty( $instance['class_custom'] ) ? '' : apply_filters( 'widget_class_custom', $instance['class_custom'] );
        $url                = isset( $instance['url'] ) ? $instance['url'] : '';
        $width              = isset( $instance['width'] ) ? $instance['width'] : '';
        $height             = isset( $instance['height'] ) ? $instance['height'] : '';
        $use_small_header   = isset( $instance['use_small_header'] ) ? $instance['use_small_header'] : '';
        $adapt_to_container = isset( $instance['adapt_to_container'] ) ? $instance['adapt_to_container'] : '';
        $hide_cover         = isset( $instance['hide_cover'] ) ? $instance['hide_cover'] : '';
        $show_face          = isset( $instance['show_face'] ) ? $instance['show_face'] : '';
        $show_posts         = isset( $instance['show_posts'] ) ? $instance['show_posts'] : '';
        $output = '';
        ob_start();
        ?>
        <?php echo wp_kses_post($args['before_widget']); ?>
        <?php if ($title) {
            echo wp_kses_post($args['before_title'] . $title . $args['after_title']);
        } ?>
        <div class="fb-page"
             data-href="<?php echo esc_url( $url ) ?>"
             <?php
             if( $show_posts == '1' ){
                 echo 'data-tabs="timeline"';
             }
             ?>
             data-width="<?php echo $width ?>"
             data-height="<?php echo $height ?>"
             data-small-header="<?php echo $use_small_header == '1' ? 'true' : 'false' ?>"
             data-adapt-container-width="<?php echo $adapt_to_container == '1' ? 'true' : 'false' ?>"
             data-hide-cover="<?php echo $hide_cover == '1' ? 'true' : 'false' ?>"
             data-show-facepile="<?php echo $show_face == '1' ? 'true' : 'false' ?>">

            <div class="fb-xfbml-parse-ignore">
                <blockquote cite="<?php echo esc_url( $url ) ?>">
                    <a href="<?php echo esc_url( $url ) ?>"><?php echo esc_url( $url ) ?></a>
                </blockquote>
            </div>
        </div>

        <?php echo wp_kses_post($args['after_widget']); ?>
        <?php
        $content =  ob_get_clean();
        echo $content;
        $this->cache_widget( $args, $content );
    }
}


if (!function_exists('hemelios_register_widget_facebook')) {
    function hemelios_register_widget_facebook() {
        register_widget('Hemelios_Widget_Facebook');
    }
    add_action('widgets_init', 'hemelios_register_widget_facebook', 1);
}