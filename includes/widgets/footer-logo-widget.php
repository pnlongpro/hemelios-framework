<?php
/**
 * Created by PhpStorm.
 * User: phuongth
 * Date: 3/26/15
 * Time: 5:24 PM
 */
class hemelios_Footer_Logo extends  G5Plus_Widget {
    public function __construct() {
        $this->widget_cssclass    = 'widget-footer-logo';
        $this->widget_description = __( "Logo and sub description", 'hemelios' );
        $this->widget_id          = 'hemelios-footer-logo';
        $this->widget_name        = __( 'Hemelios - Footer Logo', 'hemelios' );
        $this->settings           = array(
            'logo_image'  => array(
                'type'  => 'image',
                'std'   => '',
                'label' => __( 'Upload Logo', 'hemelios' )
            ),
            'sub_description'  => array(
                'type'  => 'text-area',
                'std'   => '',
                'label' => __( 'Footer logo', 'hemelios' )
            )
        );
        parent::__construct();
    }

    function widget( $args, $instance ) {
        extract( $args, EXTR_SKIP );
        $sub_description  = empty( $instance['sub_description'] ) ? '' : apply_filters( 'widget_sub_description', $instance['sub_description'] );
        $logo_image  = empty( $instance['logo_image'] ) ? '' : $instance['logo_image'];
        $class_custom   = empty( $instance['class_custom'] ) ? '' : apply_filters( 'widget_class_custom', $instance['class_custom'] );
        $widget_id = $args['widget_id'];
        echo wp_kses_post($before_widget);
        global $g5plus_options;
        $footer_logo = $logo_image;

        if( ! $footer_logo ){
            if(isset($g5plus_options['light_logo'])){
                $footer_logo = $g5plus_options['light_logo']['url'];
            }
        }
        ?>
        <div class="footer-logo <?php echo esc_attr($class_custom) ?>">
            <?php if(isset($footer_logo) && $footer_logo!='') { ?>
                <a href="<?php echo get_home_url() ?>"><img src="<?php echo esc_url($footer_logo) ?>" alt="<?php __('hemelios logo','hemelios') ?>" /></a>
            <?php } ?>
            <div class="sub-description">
                <?php echo wp_kses_post($sub_description) ?>
            </div>
        </div>

        <?php
        echo wp_kses_post($after_widget);
    }
}
if (!function_exists('hemelios_register_widget_footer_logo')) {
    function hemelios_register_widget_footer_logo() {
        register_widget('hemelios_Footer_Logo');
    }
    add_action('widgets_init', 'hemelios_register_widget_footer_logo', 1);
}